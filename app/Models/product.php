<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Products extends Model
{
    use HasFactory;
    /**
     * fillable
     * @var array
     */
    protected $fillable =[
        'image','barcode','title','description','buy_price','sell_price','stock'
    ];

    /**
     * categories
     * 
     * @return void
     */
    public function categories()
    {
        return $this->BelongsTo(Categories::class);
    }

    /**
     * details
     * 
     * @return void
     */
    public function details()
    {
        return $this->HasMany(transactions_detail::class);
    }

    /**
     * image
     * 
     * @return attribute
     */
    public function image(): Attribute
    {
        return Attribute::make(
            get:fn ($value) =>asset ('/storage/products/'. $value),
        );
    }
}

