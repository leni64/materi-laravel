<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Categories extends Model
{
    use HasFactory;
    /**
     * fillable
     * @var array
     */
    protected $fillable =[
        'image','name','description'
    ];

    /**
     * products
     * 
     * @return void
     */
    public function products()
    {
        return $this->HasMany(Products::class);
    }

     /**
     * image
     * 
     * @return attribute
     */
    public function image(): Attribute
    {
        return Attribute::make(
            get:fn ($value) =>asset ('/storage/categories/'. $value),
        );
    }
}
